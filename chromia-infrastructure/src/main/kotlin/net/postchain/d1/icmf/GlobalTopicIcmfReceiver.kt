package net.postchain.d1.icmf

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.slf4j.MDCContext
import mu.KLogging
import net.postchain.base.withReadConnection
import net.postchain.common.BlockchainRid
import net.postchain.core.Shutdownable
import net.postchain.core.Storage
import net.postchain.crypto.CryptoSystem
import net.postchain.d1.client.ChromiaClientProvider
import net.postchain.d1.cluster.ClusterManagement
import net.postchain.d1.config.BlockchainConfigProvider
import net.postchain.d1.query.ChromiaQueryProvider
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap
import kotlin.time.Duration.Companion.minutes

class GlobalTopicIcmfReceiver(
        topics: Map<String, List<BlockchainRid>>,
        private val cryptoSystem: CryptoSystem,
        private val storage: Storage,
        private val queryProvider: ChromiaQueryProvider,
        private val myChainId: Long,
        private val myBlockchainRid: BlockchainRid,
        private val clusterManagement: ClusterManagement,
        private val blockchainConfigProvider: BlockchainConfigProvider,
        private val clientProvider: ChromiaClientProvider,
        private val dbOperations: IcmfDatabaseOperations
) : IcmfReceiver<TopicRoute, Long, IcmfAnchorPacket, String>, Shutdownable {
    companion object : KLogging() {
        val pollInterval = 1.minutes
    }

    private val routes = topics.map { TopicRoute(it.key, it.value) }
    private val pipes: ConcurrentMap<Pair<String, TopicRoute>, IcmfPipe<TopicRoute, Long, IcmfAnchorPacket, String>> = ConcurrentHashMap()
    private val jobSynchronizer = Object()
    private var job: Job? = null

    private fun start(): Job {
        return CoroutineScope(Dispatchers.IO).launch(CoroutineName("clusters-updater") + MDCContext()) {
            val myCluster = clusterManagement.getClusterOfBlockchain(myBlockchainRid)

            val lastMessageHeights = withReadConnection(storage, myChainId) {
                dbOperations.loadAllLastMessageHeights(it)
            }

            val allClusters = clusterManagement.getClusterNames()
            for (route in routes) {
                if (route.chains.isNotEmpty()) {
                    route.chains.mapNotNull {
                        try {
                            clusterManagement.getClusterOfBlockchain(it)
                        } catch (e: Exception) {
                            logger.warn(e) { "Global topic for blockchain rid $it ignored since cluster name lookup failed: ${e.message}" }
                            null
                        }
                    }.distinct().forEach { clusterName ->
                        pipes[clusterName to route] = createPipe(
                                myCluster,
                                clusterName,
                                route,
                                lastMessageHeights.filter { it.topic == route.topic }.map { it.sender to it.height })
                    }
                } else {
                    for (clusterName in allClusters) {
                        pipes[clusterName to route] = createPipe(
                                myCluster,
                                clusterName,
                                route,
                                lastMessageHeights.filter { it.topic == route.topic }.map { it.sender to it.height })
                    }
                }
            }

            while (isActive) {
                delay(pollInterval)
                try {
                    logger.info("Updating set of clusters")
                    updateClusters(myCluster)
                    logger.info("Updated set of clusters")
                } catch (e: CancellationException) {
                    break
                } catch (e: Exception) {
                    logger.error("Clusters update failed: ${e.message}", e)
                }
            }
        }
    }

    private fun createPipe(
            myCluster: String,
            clusterName: String,
            route: TopicRoute,
            lastMessageHeights: List<Pair<BlockchainRid, Long>>
    ): IcmfPipe<TopicRoute, Long, IcmfAnchorPacket, String> {
        return if (clusterName == myCluster) {
            IntraClusterAnchoredTopicPipe(
                    queryProvider,
                    route,
                    clusterName,
                    clusterManagement
            )
        } else {
            val lastAnchorHeight = withReadConnection(storage, myChainId) {
                dbOperations.loadLastAnchoredHeight(it, clusterName, route.topic)
            }

            InterClusterAnchoredTopicPipe(
                    route, clusterName, cryptoSystem, lastAnchorHeight, clientProvider,
                    clusterManagement, blockchainConfigProvider, lastMessageHeights
            )
        }
    }

    private fun updateClusters(myCluster: String) {
        val currentClusters = pipes.keys.map { it.first }.toSet()
        val updatedClusters = clusterManagement.getClusterNames().toSet()
        val removedClusters = currentClusters - updatedClusters
        val addedClusters = updatedClusters - currentClusters
        for (clusterName in removedClusters) {
            for (route in routes) {
                pipes.remove(clusterName to route)?.shutdown()
            }
        }
        for (clusterName in addedClusters) {
            for (route in routes) {
                pipes[clusterName to route] = createPipe(myCluster, clusterName, route, listOf())
            }
        }
    }

    override fun getRelevantPipes(): List<IcmfPipe<TopicRoute, Long, IcmfAnchorPacket, String>> {
        synchronized(jobSynchronizer) {
            if (job == null) {
                try {
                    job = start()
                } catch (e: Exception) {
                    logger.error("Failed to start receiver", e)
                    shutdown() // Clean up
                }
            }
        }
        return pipes.values.toList()
    }

    override fun shutdown() {
        synchronized(jobSynchronizer) {
            job?.cancel()
        }
        for (pipe in pipes.values) {
            pipe.shutdown()
        }
    }
}
