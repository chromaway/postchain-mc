// Copyright (c) 2022 ChromaWay AB. See README for license information.

package net.postchain.d1.icmf

import net.postchain.common.BlockchainRid
import net.postchain.gtv.Gtv
import net.postchain.gtv.merkle.GtvMerkleHashCalculatorBase

data class IcmfMessage(
        val body: Gtv,
        val size: Int
)

/**
 * Conceptually = all messages related to a block
 */
data class IcmfPacket(
        val height: Long, // Block height this package corresponds to
        val sender: BlockchainRid,
        val topic: String,
        val blockRid: ByteArray, // The BlockRid that goes with the header (for the cases where we cannot calculate it from the header)
        val rawHeader: ByteArray, // Header of the block
        val rawWitness: ByteArray, // Must send the witness so the recipient can validate
        val prevMessageBlockHeight: Long,
        val merkleHashCalculator: GtvMerkleHashCalculatorBase,
        val messages: List<IcmfMessage> // (potentially) messages
)

data class IcmfAnchorPacket(
        val rawAnchorHeader: ByteArray,
        val rawAnchorWitness: ByteArray,
        val height: Long,
        val packets: List<IcmfPacket>
)

data class IcmfPackets<PtrT, PktT>(
        val currentPointer: PtrT,
        val packets: List<PktT>
)
