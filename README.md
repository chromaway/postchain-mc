# Postchain Chromia

Postchain Chromia provides the infrastructure for a Chromia node.

More information can be found [here](doc).

Technical information about Postchain can be found [here](https://gitlab.com/chromaway/postchain/-/tree/dev/doc).

## Copyright & License information

Copyright (c) 2017–2023 ChromaWay AB. All rights reserved.

This software can be used either under the terms of commercial license
obtained from ChromaWay AB, or, alternatively, under the terms
of the GNU General Public License with additional linking exceptions.
See file LICENSE for details.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.