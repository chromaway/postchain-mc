# How do I vote?

Providers together decide on events on the network. Power is decentralized. To vote on a proposal you need to know 
its identity:

```shell
pmc proposal list
pmc proposal info --id <id>
pmc proposal vote --id --accept
pmc proposal vote --id --reject
```
