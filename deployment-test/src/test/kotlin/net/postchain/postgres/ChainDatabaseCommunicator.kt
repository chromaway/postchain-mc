package net.postchain.postgres

import net.postchain.api.internal.BlockchainApi
import net.postchain.base.BaseEContext
import net.postchain.base.data.DatabaseAccess
import net.postchain.base.data.DatabaseAccessFactory
import net.postchain.common.BlockchainRid
import org.apache.commons.dbcp2.BasicDataSource
import org.awaitility.Duration
import org.awaitility.kotlin.await

class ChainDatabaseCommunicator(chainIId: Long, schema: String, dbConfig: DatabaseConfig) {

    private val dbAccess = DatabaseAccessFactory.createDatabaseAccess(dbConfig.driverClassName)
    private val dataSource = BasicDataSource().apply {
        addConnectionProperty("currentSchema", schema)
        driverClassName = dbConfig.driverClassName
        url = dbConfig.jdbcUrl
        username = dbConfig.username
        password = dbConfig.password
        defaultAutoCommit = true
        maxTotal = 5
        defaultReadOnly = true
    }

    private val eContext = BaseEContext(dataSource.connection, chainIId, dbAccess)

    fun getHeight(): Long {
        return DatabaseAccess.of(eContext).getLastBlockHeight(eContext)
    }

    fun awaitBlockHeight(height: Long, timeOut: Duration = Duration.TWO_MINUTES) {
        await.pollInterval(Duration.TEN_SECONDS).atMost(timeOut).until {
            getHeight() >= height
        }
    }

    fun getChainId(blockchainRid: BlockchainRid): Long? {
        return DatabaseAccess.of(eContext).getChainId(eContext, blockchainRid)
    }

    fun isChainArchivedOnNode(blockchainRid: BlockchainRid): Boolean {
        val chainId = getChainId(blockchainRid) ?: return false
        return dataSource.connection.use {
            val chainEContext = BaseEContext(it, chainId, dbAccess)
            BlockchainApi.isBlockchainArchivedOnNode(chainEContext)
        }
    }
}
